# projet 7 : Développez un algorithme de recherche en JavaScript

Un projet dans le cadre d'un projet Openclassroom. 

## Description

Le but était de creer 2 algorithmes en javascript vanilla afin de filtrer une liste d'ingredient donnée en JSON.
Pour se faire, aucun framwork n'était autorisé.


## Organisation

Le projet est organisé sous différents dossiers : 

### data

Regroupe la base de donnée en JSON

### css 

Ce sont les fichiers css utilisés

### scss

Ce sont les scripts scss

### img 

Ce sont les documents png & jpg

### js

Les différents scripts js utilisés 

### main.js & main2.js

Les deux algorithmes creés pour filtrer la logique de recherche

## Les diagrammes des deux algorithmes 

### Algorithme 1
![algo 1](/docs/algo_1_diagram.png)

### Algorithme 2
![algo 2](/docs/algo_2_diagram.png)

## Test JSBENCH
![jsbench](/docs/jsbench.png)

Je me suis trompé de nom lors du test algo1 == Algorithme 2 & algo2 == Algorithme 1