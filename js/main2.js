//------------------- Global variables used -----------------------//
let mainBar = [];
let ingredients = [];
let appliances = [];
let ustensils = [];
let allRecipes = [];
//----------------------------------------------------------------

//-------- Initial script -----------//
loadJSON(function(response) {
    const getAllrecipes = JSON.parse(response);
    const recipes = getAllrecipes.recipes;
    const allData = pretreatData(recipes);
    allRecipes = allData;

    fillRecipes(allData);
    HTML_filterBox("ingredients", getFilter(allData, "ingredients"));
    HTML_filterBox("appliances", getFilter(allData, "appliances"));
    HTML_filterBox("ustensils", getFilter(allData, "ustensils"));


    //---When enter key on main searchbar
    const serachBox = document.getElementById("searchbar");
    serachBox.addEventListener("keyup", (key) => {
        search(allData);
    });


    //---When search specific filter
    serachBox_primary.addEventListener("keyup", (key) => {
        const value = key.target.value.toLowerCase();

        if (value.length >= 3) {
            HTML_filterBox(
                "ingredients",
                getFilter(allData, "ingredients", value)
            );
        } else {
            HTML_filterBox("ingredients", getFilter(allData, "ingredients"));
        }
    });


    serachBox_secondary.addEventListener("keyup", (key) => {
        const value = key.target.value.toLowerCase();

        if (value.length >= 3) {
            HTML_filterBox(
                "appliances",
                getFilter(allData, "appliances", value)
            );
        } else {
            HTML_filterBox("appliances", getFilter(allData, "appliances"));
        }
    });

    
    serachBox_tiertary.addEventListener("keyup", (key) => {
        const value = key.target.value.toLowerCase();

        if (value.length >= 3) {
            HTML_filterBox("ustensils", getFilter(allData, "ustensils", value));
        } else {
            HTML_filterBox("ustensils", getFilter(allData, "ustensils"));
        }
    });
});

function pretreatData(rawData) {
    let data = [];
    rawData.forEach(recipe => {
        data.push({
            "name": recipe.name.toLowerCase(),
            "description": recipe.description.toLowerCase(),
            "appliance": recipe.appliance.toLowerCase(),
            "ingredients": [...recipe.ingredients.map(ingredient => {return  {"ingredient": ingredient.ingredient.toLowerCase(), "quantity": ingredient.quantity, "unit": ingredient.unit} } )],
            "ustensils": recipe.ustensils,
            "time": recipe.time,
            "raw": recipe,
        })
    })

    return data;
}