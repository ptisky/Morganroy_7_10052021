/**
 * Html generate for filterbox
 * @param {String} type
 * @param {Array} datas
 */
function HTML_filterBox(type, datas) {
    const target = document.querySelector(".options_container." + type);
    RESET_filterBox(target);
    datas.forEach((data) => {
        const li = document.createElement("li");
        const node = document.createTextNode(data);
        li.classList.add("option");
        li.setAttribute("data-value", data);
        li.appendChild(node);
        target.appendChild(li);

        li.addEventListener("click", function () {
            addFilter(li, type);
            search(allRecipes);
        });
    });
}

/**
 * reset html filterbox
 * @param {String} target
 */
function RESET_filterBox(target) {
    target.innerHTML = "";
}

/**
 * Html generate for filter pills
 * @param {String} type
 * @param {Array} datas
 */
function HTML_filterPill(type, data) {
    const target = document.querySelector("#filter_added");

    const li = document.createElement("li");
    const node = document.createTextNode(data);
    li.classList.add("filter_selected", type);
    li.setAttribute("data-value", data);
    li.appendChild(node);
    target.appendChild(li);

    const img = document.createElement("img");
    img.classList.add("remove_filter");
    img.setAttribute("src", "img/cross.svg");
    li.appendChild(img);

    img.addEventListener("click", function () {
        deleteFilter(li, type);
        search(allRecipes);
    });
}

/**
 * Display error message when no recipes founded
 */
 function noRecipes() {
    let pElement = document.createElement("p");
    pElement.innerHTML =
        "Aucune recette ne correspond à votre critère… vous pouvez chercher « tarte aux pommes », « poisson », etc.";
    document.getElementById("recipe").append(pElement);
}
